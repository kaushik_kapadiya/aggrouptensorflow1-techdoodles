package com.group.ag.ag_group_tensorflow4android_v11.fragments;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.activities.DashboardActivity;
import com.group.ag.ag_group_tensorflow4android_v11.camera.AutoFitTextureView;
import com.group.ag.ag_group_tensorflow4android_v11.camera.MeritAppCamera;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Utils;

import java.io.File;

public class MomentCameraFragment extends Fragment implements Constants {
    private DashboardActivity mActivity;
    private AutoFitTextureView mTextureView;
    private ImageView mImgCapturePhoto;
    private MeritAppCamera mMeritAppCamera;
    private boolean mIsFrontCamera = false;
    private ImageView mImgBackArrow;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_moment_camera, container, false);
        mActivity = (DashboardActivity) getActivity();
        mTextureView = view.findViewById(R.id.textureView);
        mImgCapturePhoto = view.findViewById(R.id.btnCapturePhoto);

        mImgCapturePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showProgressDialog(mActivity);
                mMeritAppCamera.takePicture();
            }
        });
        ImageView imgChangeCam = view.findViewById(R.id.btnChangeCam);
        imgChangeCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsFrontCamera = !mIsFrontCamera;
                startCamera();
            }
        });

        mImgBackArrow = view.findViewById(R.id.imgBackArrow);
        mImgBackArrow.setVisibility(View.VISIBLE);
        mImgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        mIsFrontCamera = false;
        startCamera();
        return view;

    }


    private void startCamera() {
        Log.e("STATE", "startCamera called");
        if (mMeritAppCamera != null) {
            mMeritAppCamera.closeCamera();
            mMeritAppCamera.stopBackgroundThread();
        }
        mMeritAppCamera = new MeritAppCamera(getActivity(), mTextureView);
        mMeritAppCamera.startBackgroundThread();
        Log.e("STATE", "startCamera camera starts");
        mMeritAppCamera.setCameraCallback(new MeritAppCamera.CameraCallback() {
            @Override
            public void onPicture(final File file, final Bitmap croppedBitmap) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.dismissProgressDialog();
                        if (mMeritAppCamera != null) {
                            mMeritAppCamera.closeCamera();
                            mMeritAppCamera.stopBackgroundThread();
                            mMeritAppCamera = null;
                        }
                        String path = Utils.saveCroppedBitmap(croppedBitmap);
                        mActivity.goToPicCropFragment(path);
                    }
                });
            }

            @Override
            public void removeOrientationListener() {
            }

            @Override
            public void onError(String message) {

            }
        });
        if (mTextureView.isAvailable()) {
            Log.e("log_tag", "startCamera mTextureView.isAvailable()");
            mMeritAppCamera.openCamera(mTextureView.getWidth(), mTextureView.getHeight(), mIsFrontCamera);
        } else {
            Log.e("log_tag", "startCamera mTextureView not isAvailable()");
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            Log.e("log_tag", "onSurfaceTextureAvailable width : " + width + " Height : " + height);
            startCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            mMeritAppCamera.configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }

    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("STATE", "onDestroyView MomentCamera");
        if (mMeritAppCamera != null) {
            mMeritAppCamera.closeCamera();
            mMeritAppCamera.stopBackgroundThread();
            mMeritAppCamera = null;
        }
    }

}
