package com.group.ag.ag_group_tensorflow4android_v11.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.group.ag.ag_group_tensorflow4android_v11.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;


public class Utils implements Constants {

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean saveStyledBitmap(Bitmap bitmap, String fileName) {
        File styledPhotosFolder = getStyledPhotosFolder();

        File styledImage = new File(styledPhotosFolder, fileName);
        if (styledImage.exists()) {
            styledImage.delete();
        }
        try {
            OutputStream outStream = new FileOutputStream(styledImage);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String saveCroppedBitmap(Bitmap bitmap) {
        File styledPhotosFolder = getCameraPhotosFolder();

        File croppedImage = new File(styledPhotosFolder, "Crop.jpg");
        if (croppedImage.exists()) {
            croppedImage.delete();
        }
        try {
            OutputStream outStream = new FileOutputStream(croppedImage);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            return croppedImage.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static File getStyledPhotosFolder() {
        File parentFolder = new File(Environment.getExternalStorageDirectory(), FOLDER_PARENT_PHOTOS);
        if (!parentFolder.exists()) {
            parentFolder.mkdirs();
        }
        File styledPhotosFolder = new File(parentFolder, FOLDER_STYLED_PHOTOS);
        if (!styledPhotosFolder.exists()) {
            styledPhotosFolder.mkdirs();
        }
        return styledPhotosFolder;
    }

    public static File getCameraPhotosFolder() {
        File parentFolder = new File(Environment.getExternalStorageDirectory(), FOLDER_PARENT_PHOTOS);
        if (!parentFolder.exists()) {
            parentFolder.mkdirs();
        }
        File styledPhotosFolder = new File(parentFolder, FOLDER_CAMERA_CLICKED_PHOTOS);
        if (!styledPhotosFolder.exists()) {
            styledPhotosFolder.mkdirs();
        }
        return styledPhotosFolder;
    }

    private static Dialog mProgressDialog;

    public static void showProgressDialog(Context mContext) {
        mProgressDialog = new Dialog(mContext);
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.setContentView(R.layout.dialog_progress);
        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public static void showMessageDialog(Context mContext, int message) {
        final Dialog messageDialog = new Dialog(mContext);
        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(R.layout.dialog_message);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.setCancelable(false);

        TextView textMessage = messageDialog.findViewById(R.id.textDialogMessage);
        textMessage.setText(message);
        messageDialog.findViewById(R.id.textOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messageDialog.dismiss();
            }
        });
        messageDialog.show();
    }

}
