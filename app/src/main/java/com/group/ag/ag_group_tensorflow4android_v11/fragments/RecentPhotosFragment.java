package com.group.ag.ag_group_tensorflow4android_v11.fragments;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.activities.DashboardActivity;
import com.group.ag.ag_group_tensorflow4android_v11.adapters.StyledPhotosAdapter;
import com.group.ag.ag_group_tensorflow4android_v11.models.PhotoDTO;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;
import com.group.ag.ag_group_tensorflow4android_v11.utils.OnRecyclerItemClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


public class RecentPhotosFragment extends Fragment implements Constants {
    private DashboardActivity mActivity;
    private View mView;
    private ImageView mImgBackArrow;
    private RecyclerView mRecycleStyledPhotos;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_recent_photos, container, false);
            mActivity = (DashboardActivity) getActivity();
        }
        initControls();
        return mView;
    }

    private void initControls() {
        mImgBackArrow = mView.findViewById(R.id.imgBackArrow);
        mImgBackArrow.setVisibility(View.VISIBLE);
        mImgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        mRecycleStyledPhotos = mView.findViewById(R.id.recycleStyledPhotos);

        GridLayoutManager manager = new GridLayoutManager(mActivity, 3);
        mRecycleStyledPhotos.setLayoutManager(manager);

        final ArrayList<PhotoDTO> mData = getRecentPhotos();
        StyledPhotosAdapter mStyledPhotosAdapter = new StyledPhotosAdapter(mActivity, mData);
        mStyledPhotosAdapter.setOnItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mActivity.goToPicPreviewFragment(mData.get(position).imagePath);
            }
        });
        mRecycleStyledPhotos.setAdapter(mStyledPhotosAdapter);
    }

    private ArrayList<PhotoDTO> getRecentPhotos() {
        ArrayList<PhotoDTO> data = new ArrayList<>();
        File parentFolder = new File(Environment.getExternalStorageDirectory(), FOLDER_PARENT_PHOTOS);
        if (parentFolder.exists()) {
            File photosFolder = new File(parentFolder, FOLDER_STYLED_PHOTOS);
            if (photosFolder.exists()) {
                File[] files = photosFolder.listFiles();
                for (File imageFile : files) {
                    if (imageFile.isFile()) {
                        Log.e("getRecentPhotos", "ImageFile Path : " + imageFile.getAbsolutePath());
                        Date lastModDate = new Date(imageFile.lastModified());
                        data.add(new PhotoDTO(imageFile.getAbsolutePath(), lastModDate));
                    }
                }
            }
        }
        Collections.sort(data, new Comparator<PhotoDTO>() {
            public int compare(PhotoDTO o1, PhotoDTO o2) {
                if (o1.date == null || o2.date == null)
                    return 0;
                return o2.date.compareTo(o1.date);
            }
        });
        return data;
    }

}
