package com.group.ag.ag_group_tensorflow4android_v11.models;

import java.io.Serializable;
import java.util.Date;


public class PhotoDTO implements Serializable {
    public String imagePath = "";
    public Date date;
    public PhotoDTO(String path, Date date) {
        this.imagePath = path;
        this.date = date;
    }
}
