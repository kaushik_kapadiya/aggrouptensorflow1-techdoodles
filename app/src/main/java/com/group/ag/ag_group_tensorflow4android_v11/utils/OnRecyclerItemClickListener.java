package com.group.ag.ag_group_tensorflow4android_v11.utils;


public interface OnRecyclerItemClickListener {
    void onItemClick(int position);
}
