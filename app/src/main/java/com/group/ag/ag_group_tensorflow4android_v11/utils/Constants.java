package com.group.ag.ag_group_tensorflow4android_v11.utils;


public interface Constants {
//    int INPUT_SIZE = 256;

    String MODEL_FILE_B = "file:///android_asset/bossK_float.pb";
    String MODEL_FILE_C = "file:///android_asset/cubist_float.pb";
    String MODEL_FILE_D = "file:///android_asset/denoised_starry_float.pb";
    String MODEL_FILE_F = "file:///android_asset/feathers_float.pb";
    String MODEL_FILE_M = "file:///android_asset/mosaic_float.pb";
    String MODEL_FILE_S = "file:///android_asset/scream_float.pb";
    String MODEL_FILE_U = "file:///android_asset/udnie_float.pb";
    String MODEL_FILE_W = "file:///android_asset/wave_float.pb";
    String MODEL_FILE_CR = "file:///android_asset/crayon_float.pb";
    String MODEL_FILE_I = "file:///android_asset/ink_float.pb";


    String FOLDER_PARENT_PHOTOS = "AGGroupPhotos";
    String FOLDER_CAMERA_CLICKED_PHOTOS = "AGGroupCameraPhotos";
    String FOLDER_STYLED_PHOTOS = "AGGroupStyledPhotos";

    String CAMERA_CLICKED_PHOTO_NAME = "AG_CAMERA_PIC_%1$s.jpg";
    String STYLED_PHOTO_NAME = "AG_STYLED_PIC_%1$s.jpg";

    String EXTRA_KEY_IMAGE_PATH = "IMAGE_PATH";
}
