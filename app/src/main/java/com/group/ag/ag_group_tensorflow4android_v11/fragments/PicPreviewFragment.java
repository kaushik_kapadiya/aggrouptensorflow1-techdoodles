package com.group.ag.ag_group_tensorflow4android_v11.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdListener;
import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.activities.DashboardActivity;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Utils;

import java.io.File;


public class PicPreviewFragment extends Fragment implements Constants {
    private DashboardActivity mActivity;
    private View mView;
    private ImageView mImgBackArrow, mImgSelectedPhoto, mImgSharePhoto;
    private String mSelectedImagePath = "";


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_pic_preview, container, false);
            mActivity = (DashboardActivity) getActivity();
        }
        mSelectedImagePath = getArguments().getString(EXTRA_KEY_IMAGE_PATH);
        initControls();
        return mView;
    }


    private void initControls() {
        mImgBackArrow = mView.findViewById(R.id.imgBackArrow);
        mImgBackArrow.setVisibility(View.VISIBLE);
        mImgSharePhoto = mView.findViewById(R.id.imgSharePhoto);
        mImgSharePhoto.setVisibility(View.VISIBLE);
        ImageView imgDeletePhoto = mView.findViewById(R.id.imgDeletePhoto);
        imgDeletePhoto.setVisibility(View.VISIBLE);
        imgDeletePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteConfirmDialog();
            }
        });

        mImgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        mImgSharePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharePhoto();

            }
        });
        mImgSelectedPhoto = mView.findViewById(R.id.imgSelectedPhoto);
        Glide.with(mActivity)
                .load(Uri.parse("file://" + mSelectedImagePath)) // Uri of the picture
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(mImgSelectedPhoto);
    }

    private void showDeleteConfirmDialog() {
        final Dialog mProgressDialog = new Dialog(mActivity);
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.setContentView(R.layout.dialog_delete_confirm);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        mProgressDialog.findViewById(R.id.textYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(mSelectedImagePath);
                boolean deleted = file.delete();
                mProgressDialog.dismiss();
                if (deleted) {
                    mActivity.onBackPressed();
                } else {
                    Utils.showToast(mActivity, "Something went wrong");
                }
            }
        });
        mProgressDialog.findViewById(R.id.textNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.dismiss();
            }
        });
        mProgressDialog.show();
    }


    private void sharePhoto() {
        if (mActivity.getAd().isLoaded()) {
            mActivity.getAd().show();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    File stylePhotoFile = new File(mSelectedImagePath);
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    File stylePhotoFile = new File(mSelectedImagePath);
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }
            });
        } else {
            mActivity.loadAd();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    File stylePhotoFile = new File(mSelectedImagePath);
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mActivity.getAd().show();
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    File stylePhotoFile = new File(mSelectedImagePath);
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }
            });

        }
    }
}
