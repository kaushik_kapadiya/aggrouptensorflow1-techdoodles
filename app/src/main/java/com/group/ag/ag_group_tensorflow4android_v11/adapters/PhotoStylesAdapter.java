package com.group.ag.ag_group_tensorflow4android_v11.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.models.StyleDTO;
import com.group.ag.ag_group_tensorflow4android_v11.utils.OnRecyclerItemClickListener;

import java.util.ArrayList;

public class PhotoStylesAdapter extends RecyclerView.Adapter<PhotoStylesAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<StyleDTO> mData;
    private OnRecyclerItemClickListener mItemClickListener;

    public PhotoStylesAdapter(Context context, ArrayList<StyleDTO> list) {
        this.mContext = context;
        this.mData = list;
    }


    public void setOnItemClickListener(final OnRecyclerItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                mContext).inflate(R.layout.row_photo_style, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        StyleDTO dto = mData.get(position);
        holder.imgPhotoStyle.setImageResource(dto.resId);
        holder.textStyleName.setText(dto.styleName);
        holder.imgPhotoStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhotoStyle;
        TextView textStyleName;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgPhotoStyle = (ImageView) itemView.findViewById(R.id.imgPhotoStyle);
            textStyleName = itemView.findViewById(R.id.textStyleName);
        }
    }
}
