package com.group.ag.ag_group_tensorflow4android_v11.fragments;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Trace;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdListener;
import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.activities.DashboardActivity;
import com.group.ag.ag_group_tensorflow4android_v11.adapters.PhotoStylesAdapter;
import com.group.ag.ag_group_tensorflow4android_v11.models.StyleDTO;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;
import com.group.ag.ag_group_tensorflow4android_v11.utils.OnRecyclerItemClickListener;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Utils;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class PicEditFragment300160 extends Fragment implements Constants {
    private static final String INPUT_NAME = "input";
    private static final String INPUT_T_NAME = "input_t";
    private static final String OUTPUT_T_NAME = "out_t";

    private DashboardActivity mActivity;
    private View mView;
    private ImageView mImgBackArrow, mImgSelectedPhoto, mImgSavePhoto, mImgSharePhoto, mImgEditPhoto;
    private RecyclerView mRecycleStyles;
    private boolean mIsStyleApplied = false;
    private String mSelectedImagePath = "";
    private Bitmap mStyledBitmap;
    private String mCurrentFileName = "";

    private Matrix matrix;
    private int[] intValues;
    private float[] floatValues;
    private TensorFlowInferenceInterface inferenceInterface;
    //    private int INPUT_SIZE_WIDTH = 300, INPUT_SIZE_HEIGHT = 160, INPUT_SIZE = 256;
    private int INPUT_SIZE_WIDTH = 256, INPUT_SIZE_HEIGHT = 256, INPUT_SIZE = 256;
//    private int INPUT_SIZE_WIDTH = 512, INPUT_SIZE_HEIGHT = 512, INPUT_SIZE = 512;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_pic_edit, container, false);
            mActivity = (DashboardActivity) getActivity();
        }
        mSelectedImagePath = getArguments().getString(EXTRA_KEY_IMAGE_PATH);
        initControls();
        return mView;
    }

    private void initControls() {
        mCurrentFileName = String.format(STYLED_PHOTO_NAME, System.currentTimeMillis());

        matrix = new Matrix();
        matrix.postScale(1f, 1f);

        intValues = new int[INPUT_SIZE_WIDTH * INPUT_SIZE_HEIGHT];
        floatValues = new float[INPUT_SIZE_WIDTH * INPUT_SIZE_HEIGHT * 3];

        mImgBackArrow = mView.findViewById(R.id.imgBackArrow);
        mImgSavePhoto = mView.findViewById(R.id.imgSavePhoto);
        mImgSharePhoto = mView.findViewById(R.id.imgSharePhoto);
        mImgEditPhoto = mView.findViewById(R.id.imgEditPhoto);
        mImgBackArrow.setVisibility(View.VISIBLE);
        mImgSavePhoto.setVisibility(View.VISIBLE);
        mImgSharePhoto.setVisibility(View.VISIBLE);
        mImgEditPhoto.setVisibility(View.VISIBLE);

        updateButtonStyle();

        mImgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        mImgSavePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsStyleApplied && mStyledBitmap != null) {
                    savePhoto();
                }
            }
        });
        mImgEditPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performCrop();
            }
        });
        mImgSharePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsStyleApplied) {
                    sharePhoto();
                }
            }
        });


        mImgSelectedPhoto = mView.findViewById(R.id.imgSelectedPhoto);
        Log.e("PicEditFragment", "initControls mSelectedImagePath : " + mSelectedImagePath);
        Glide.with(mActivity)
                .load(Uri.parse("file://" + mSelectedImagePath)) // Uri of the picture
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(mImgSelectedPhoto);

        mRecycleStyles = mView.findViewById(R.id.recyclePhotoStyles);
        LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mRecycleStyles.setLayoutManager(manager);

        final ArrayList<StyleDTO> mData = getStyles();
        PhotoStylesAdapter mPhotoStyleAdapter = new PhotoStylesAdapter(mActivity, mData);
        mRecycleStyles.setAdapter(mPhotoStyleAdapter);
        mPhotoStyleAdapter.setOnItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onItemClick(final int position) {
                Utils.showProgressDialog(mActivity);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                StyleDTO model = mData.get(position);
                                Log.e("log_tag", "doInBackground  model.fileName : " + model.fileName);
                                inferenceInterface = new TensorFlowInferenceInterface(mActivity.getAssets(), model.fileName);
                                Log.e("log_tag", "doInBackground model get");
//                                inferenceInterface = new TensorFlowInferenceInterface(mActivity.getAssets(), model.fileName);
                                Log.e("log_tag", "doInBackground inferenceInterface loaded");
                                Bitmap bitmap = getBitmapFromPath();
//                                Log.e("log_tag", "doInBackground getBitmapFromPath");
                                bitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false);
                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//                                Log.e("log_tag", "doInBackground getBitmapFromPath created");
//                                mStyledBitmap = stylizeBitmap1(bitmap);
                                mStyledBitmap = stylizeBitmap(bitmap);
                                Log.e("log_tag", "doInBackground mStyledBitmap done");
                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mImgSelectedPhoto.setImageBitmap(mStyledBitmap);
                                        Utils.dismissProgressDialog();
                                        mIsStyleApplied = true;
                                        updateButtonStyle();
                                    }
                                });
                                return null;
                            }
                        }.execute();

                    }
                }, 200);
            }
        });
    }

    private Bitmap stylizeBitmap(Bitmap bitmap) {
        Log.e("log_tag", "stylizeImage start");
        Log.e("log_tag", "stylizeImage INPUT_SIZE_WIDTH : " + INPUT_SIZE_WIDTH + " & INPUT_SIZE_HEIGHT : " + INPUT_SIZE_HEIGHT);
        Log.e("log_tag", "stylizeImage bitmap.getWidth() : " + bitmap.getWidth());
        Log.e("log_tag", "stylizeImage bitmap.getHeight() : " + bitmap.getHeight());

        Bitmap scaledBitmap = scaleBitmap(bitmap, INPUT_SIZE_WIDTH, INPUT_SIZE_HEIGHT);

        Log.e("log_tag", "stylizeImage INPUT_SIZE_WIDTH 2 : " + INPUT_SIZE_WIDTH + " & INPUT_SIZE_HEIGHT : " + INPUT_SIZE_HEIGHT);
        scaledBitmap.getPixels(intValues, 0, scaledBitmap.getWidth(), 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight());
        scaledBitmap = scaledBitmap.copy(Bitmap.Config.ARGB_8888, true);


        for (int i = 0; i < intValues.length; ++i) {
            final int val = intValues[i];
            floatValues[i * 3 + 0] = ((val >> 16) & 0xFF) * 1.0f;
            floatValues[i * 3 + 1] = ((val >> 8) & 0xFF) * 1.0f;
            floatValues[i * 3 + 2] = (val & 0xFF) * 1.0f;
        }
        Log.e("log_tag", "stylizeImage Step 1");
        Trace.beginSection("feed");
        inferenceInterface.feed(INPUT_T_NAME, floatValues, INPUT_SIZE_HEIGHT, INPUT_SIZE_WIDTH, 3);
        Trace.endSection();
        Log.e("log_tag", "stylizeImage Step 2");
        Trace.beginSection("run");
        inferenceInterface.run(new String[]{OUTPUT_T_NAME});
        Trace.endSection();
        Log.e("log_tag", "stylizeImage Step 3");
        Trace.beginSection("fetch");
        inferenceInterface.fetch(OUTPUT_T_NAME, floatValues);
        Trace.endSection();
        Log.e("log_tag", "stylizeImage Step 4");
        for (int i = 0; i < intValues.length; ++i) {
            intValues[i] =
                    0xFF000000
                            | (((int) (floatValues[i * 3 + 0])) << 16)
                            | (((int) (floatValues[i * 3 + 1])) << 8)
                            | ((int) (floatValues[i * 3 + 2]));
        }
        Log.e("log_tag", "stylizeImage Step 5");
        scaledBitmap.setPixels(intValues, 0, scaledBitmap.getWidth(), 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight());
        Log.e("log_tag", "stylizeImage finish");

        return scaledBitmap;
    }

    private Bitmap scaleBitmap(Bitmap origin, int newWidth, int newHeight) {
        if (origin == null) {
            return null;
        }
        int height = origin.getHeight();
        int width = origin.getWidth();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Log.e("scaleBitmap", "height : " + height);
        Log.e("scaleBitmap", "width : " + width);
        Log.e("scaleBitmap", "newWidth : " + newWidth);
        Log.e("scaleBitmap", "newHeight : " + newHeight);
        Log.e("scaleBitmap", "scaleWidth : " + scaleWidth);
        Log.e("scaleBitmap", "scaleHeight : " + scaleHeight);

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap newBitmap = Bitmap.createBitmap(origin, 0, 0, width, height, matrix, false);
        if (!origin.isRecycled()) {
            origin.recycle();
        }
        return newBitmap;
    }

    private void savePhoto() {
        if (mActivity.getAd().isLoaded()) {
            mActivity.getAd().show();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    if (Utils.saveStyledBitmap(mStyledBitmap, mCurrentFileName)) {
                        Utils.showToast(mActivity, "Photo Saved");
                    } else {
                        Utils.showToast(mActivity, "Photo not saved");
                    }
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    if (Utils.saveStyledBitmap(mStyledBitmap, mCurrentFileName)) {
                        Utils.showToast(mActivity, "Photo Saved");
                    } else {
                        Utils.showToast(mActivity, "Photo not saved");
                    }
                }

            });
        } else {
            mActivity.loadAd();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    if (Utils.saveStyledBitmap(mStyledBitmap, mCurrentFileName)) {
                        Utils.showToast(mActivity, "Photo Saved");
                    } else {
                        Utils.showToast(mActivity, "Photo not saved");
                    }

                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mActivity.getAd().show();
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    if (Utils.saveStyledBitmap(mStyledBitmap, mCurrentFileName)) {
                        Utils.showToast(mActivity, "Photo Saved");
                    } else {
                        Utils.showToast(mActivity, "Photo not saved");
                    }

                }
            });

        }
    }

    private void sharePhoto() {
        if (mActivity.getAd().isLoaded()) {
            mActivity.getAd().show();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    File stylePhotoFile = new File(Utils.getStyledPhotosFolder(), mCurrentFileName);
                    if (!stylePhotoFile.exists()) {
                        mImgSavePhoto.performClick();
                    }
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    File stylePhotoFile = new File(Utils.getStyledPhotosFolder(), mCurrentFileName);
                    if (!stylePhotoFile.exists()) {
                        mImgSavePhoto.performClick();
                    }
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }
            });
        } else {
            mActivity.loadAd();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    File stylePhotoFile = new File(Utils.getStyledPhotosFolder(), mCurrentFileName);
                    if (!stylePhotoFile.exists()) {
                        mImgSavePhoto.performClick();
                    }
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mActivity.getAd().show();
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    File stylePhotoFile = new File(Utils.getStyledPhotosFolder(), mCurrentFileName);
                    if (!stylePhotoFile.exists()) {
                        mImgSavePhoto.performClick();
                    }
                    Uri uri = Uri.fromFile(stylePhotoFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("image/*");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }
            });

        }
    }


    private ArrayList<StyleDTO> getStyles() {
        ArrayList<StyleDTO> mData = new ArrayList<>();
        TypedArray styles = mActivity.getResources().obtainTypedArray(R.array.AllStyles1);
        String[] styleNames = mActivity.getResources().getStringArray(R.array.AllStyleName1);
        String[] stylePaths = mActivity.getResources().getStringArray(R.array.AllStylePath1);
        for (int i = 0; i < styles.length(); i++) {
            mData.add(new StyleDTO(i, styles.getResourceId(i, 0), stylePaths[i], styleNames[i]));
        }

        return mData;
    }

    private Bitmap getBitmapFromPath() {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        try {
            ExifInterface ei = new ExifInterface(mSelectedImagePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            int rotation = 0;
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotation = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotation = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotation = 270;
                    break;
            }
            matrix.postRotate(rotation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeFile(mSelectedImagePath, bmOptions);
    }


    private void updateButtonStyle() {
        if (mIsStyleApplied) {
            mImgSavePhoto.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorBlack), android.graphics.PorterDuff.Mode.SRC_IN);
            mImgSharePhoto.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorBlack), android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            mImgSavePhoto.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorDisableOption), android.graphics.PorterDuff.Mode.SRC_IN);
            mImgSharePhoto.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorDisableOption), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void performCrop() {
        mActivity.goToPicCropFragment(mSelectedImagePath);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
