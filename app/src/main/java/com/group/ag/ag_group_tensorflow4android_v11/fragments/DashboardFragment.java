package com.group.ag.ag_group_tensorflow4android_v11.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.activities.DashboardActivity;
import com.group.ag.ag_group_tensorflow4android_v11.adapters.RecentPhotosAdapter;
import com.group.ag.ag_group_tensorflow4android_v11.models.PhotoDTO;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;
import com.group.ag.ag_group_tensorflow4android_v11.utils.ImageFilePath;
import com.group.ag.ag_group_tensorflow4android_v11.utils.OnRecyclerItemClickListener;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


public class DashboardFragment extends Fragment implements Constants {
    private static final int REQUEST_CODE_PERMISSION_CAMERA = 1;
    private static final int REQUEST_CODE_PERMISSION_STORAGE = 2;
    private static final int REQUEST_CODE_PICK_IMAGE = 3;
    private static final int REQUEST_CODE_CAPTURE_IMAGE = 4;

    private View mView;
    private ImageView mLLGallery, mLLCamera;
    private RelativeLayout mRLRecentPhotos;
    private TextView mTextAll;
    private RecyclerView mRecycleRecentPhotos;
    private LinearLayout mLLBottomText;
    private DashboardActivity mActivity;

    private boolean shouldShowGallery = false;
    private Uri mImageUri;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_dashboard, container, false);
            mActivity = (DashboardActivity) getActivity();
        }
        initControls();

        isStoragePermissionGranted(false);
        return mView;
    }

    private void initControls() {
        mLLGallery = mView.findViewById(R.id.llGallery);
        mLLCamera = mView.findViewById(R.id.llCamera);
        mRLRecentPhotos = mView.findViewById(R.id.rlRecentPhotos);
        mTextAll = mView.findViewById(R.id.textAll);
        mRecycleRecentPhotos = mView.findViewById(R.id.recycleRecentPhotos);
        mLLBottomText = mView.findViewById(R.id.llBottomText);

        mLLGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted(true)) {
                    showAd(REQUEST_CODE_PERMISSION_STORAGE);
                }
            }
        });

        mLLCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCameraPermissionGranted()) {
                    showAd(REQUEST_CODE_PERMISSION_CAMERA);
                }
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mRecycleRecentPhotos.setLayoutManager(manager);
    }

    @Override
    public void onResume() {
        super.onResume();
        showRecentPhotos();
    }

    private void showRecentPhotos() {
        final ArrayList<PhotoDTO> mData = getRecentPhotos();
        if (mData.size() > 0) {
            final ArrayList<PhotoDTO> dataNew = new ArrayList<>();
            if (mData.size() > 0) {
                int limit = 4;
                if (mData.size() < 4) {
                    limit = mData.size();
                }
                for (int i = 0; i < limit; i++) {
                    dataNew.add(mData.get(i));
                }
            }

            RecentPhotosAdapter mAdapterRecentPhotos = new RecentPhotosAdapter(mActivity, dataNew);
            mRecycleRecentPhotos.setAdapter(mAdapterRecentPhotos);
            mRLRecentPhotos.setVisibility(View.VISIBLE);
            mLLBottomText.setVisibility(View.VISIBLE);
            mAdapterRecentPhotos.setOnItemClickListener(new OnRecyclerItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    mActivity.goToPicPreviewFragment(dataNew.get(position).imagePath);
                }
            });
            if (mData.size() > 4) {
                mTextAll.setVisibility(View.VISIBLE);
                mTextAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mActivity.replaceFragment(new RecentPhotosFragment());
                    }
                });
            } else {
                mTextAll.setVisibility(View.GONE);
            }
        } else {
            mRLRecentPhotos.setVisibility(View.GONE);
            mLLBottomText.setVisibility(View.GONE);
        }
    }

    private ArrayList<PhotoDTO> getRecentPhotos() {
        ArrayList<PhotoDTO> data = new ArrayList<>();
        File parentFolder = new File(Environment.getExternalStorageDirectory(), FOLDER_PARENT_PHOTOS);
        if (parentFolder.exists()) {
            File photosFolder = new File(parentFolder, FOLDER_STYLED_PHOTOS);
            if (photosFolder.exists()) {
                File[] files = photosFolder.listFiles();
                if (files != null && files.length > 0) {
                    for (File imageFile : files) {
                        if (imageFile.isFile()) {
                            Log.e("getRecentPhotos", "ImageFile Path : " + imageFile.getAbsolutePath());
                            Date lastModDate = new Date(imageFile.lastModified());
                            Log.e("getRecentPhotos", "ImageFile Date : " + lastModDate.toString());
                            data.add(new PhotoDTO(imageFile.getAbsolutePath(), lastModDate));
                        }
                    }
                }
            }
        }
        Collections.sort(data, new Comparator<PhotoDTO>() {
            public int compare(PhotoDTO o1, PhotoDTO o2) {
                if (o1.date == null || o2.date == null)
                    return 0;
                return o2.date.compareTo(o1.date);
            }
        });

        return data;
    }

    private void showAd(final int requestCode) {
        if (mActivity.getAd().isLoaded()) {
            mActivity.getAd().show();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    switch (requestCode) {
                        case REQUEST_CODE_PERMISSION_CAMERA:
                            showCamera();
                            break;
                        case REQUEST_CODE_PERMISSION_STORAGE:
                            showGallery();
                            break;
                    }
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    switch (requestCode) {
                        case REQUEST_CODE_PERMISSION_CAMERA:
                            showCamera();
                            break;
                        case REQUEST_CODE_PERMISSION_STORAGE:
                            showGallery();
                            break;
                    }
                }

            });
        } else {
            mActivity.loadAd();
            mActivity.getAd().setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    switch (requestCode) {
                        case REQUEST_CODE_PERMISSION_CAMERA:
                            showCamera();
                            break;
                        case REQUEST_CODE_PERMISSION_STORAGE:
                            showGallery();
                            break;
                    }
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mActivity.getAd().show();
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mActivity.loadAd();
                    switch (requestCode) {
                        case REQUEST_CODE_PERMISSION_CAMERA:
                            showCamera();
                            break;
                        case REQUEST_CODE_PERMISSION_STORAGE:
                            showGallery();
                            break;
                    }
                }
            });

        }
    }

    private void showGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_PICK_IMAGE);
    }

    private void showCamera() {
        mActivity.goToCameraFragment();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PICK_IMAGE:
                if (resultCode == mActivity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String picturePath = ImageFilePath.getPath(mActivity, selectedImage);
                    File currentFile = new File(picturePath);
                    if (currentFile.getName().startsWith(STYLED_PHOTO_NAME.split("%")[0]) && currentFile.getParentFile().getName().equals(FOLDER_STYLED_PHOTOS)) {
                        mActivity.goToPicPreviewFragment(picturePath);
                    } else {
                        mActivity.goToPicCropFragment(picturePath);
                    }
                }
                break;
            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode == mActivity.RESULT_OK) {
                    Uri selectedImage = mImageUri;
                    String picturePath = ImageFilePath.getPath(mActivity, selectedImage);
                    mActivity.goToPicCropFragment(picturePath);
                }
                break;
        }
    }

    private boolean isCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int hasCameraPermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA);
            if (hasCameraPermission == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                String[] permission = {Manifest.permission.CAMERA};
                requestPermissions(permission, REQUEST_CODE_PERMISSION_CAMERA);
                return false;
            }
        }
        return true;
    }

    private boolean isStoragePermissionGranted(boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int hasReadStoragePermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE);
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasReadStoragePermission == PackageManager.PERMISSION_GRANTED && hasWriteStoragePermission == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                shouldShowGallery = show;
                String[] permission = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(permission, REQUEST_CODE_PERMISSION_STORAGE);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_CAMERA:
                int hasCameraPermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA);
                if (hasCameraPermission == PackageManager.PERMISSION_GRANTED) {
                    showAd(requestCode);
                } else {
                    Utils.showMessageDialog(mActivity, R.string.msg_denied_camera_permission);
                }
                break;
            case REQUEST_CODE_PERMISSION_STORAGE:
                int hasReadStoragePermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE);
                int hasWriteStoragePermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (hasReadStoragePermission == PackageManager.PERMISSION_GRANTED && hasWriteStoragePermission == PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowGallery) {
                        showAd(requestCode);
                    }
                } else {
                    Utils.showMessageDialog(mActivity, R.string.msg_denied_storage_permission);
                }
                shouldShowGallery = false;
                break;
        }
    }
}
