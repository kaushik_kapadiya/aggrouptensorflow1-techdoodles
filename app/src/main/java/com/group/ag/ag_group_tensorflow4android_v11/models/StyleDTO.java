package com.group.ag.ag_group_tensorflow4android_v11.models;


public class StyleDTO {
    public int type;
    public int resId;
    public String fileName = "", styleName = "";

    public StyleDTO(int type, int resId, String fileName, String styleName) {
        this.type = type;
        this.resId = resId;
        this.fileName = fileName;
        this.styleName = styleName;
    }
}
