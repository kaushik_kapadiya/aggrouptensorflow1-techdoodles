package com.group.ag.ag_group_tensorflow4android_v11.fragments;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.activities.DashboardActivity;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;


public class PicCropFragment extends Fragment implements Constants {
    private DashboardActivity mActivity;
    private View mView;
    private ImageView mImgBackArrow, mImgRotatePhoto, mImgFlipPhoto;
    private TextView mTextCrop;
    private String mSelectedImagePath = "";
    private CropImageView cropImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_pic_crop, container, false);
            mActivity = (DashboardActivity) getActivity();
        }
        mSelectedImagePath = getArguments().getString(EXTRA_KEY_IMAGE_PATH);
        initControls();
        return mView;
    }


    private void initControls() {
        mImgBackArrow = mView.findViewById(R.id.imgBackArrow);
        mImgBackArrow.setVisibility(View.VISIBLE);
        mImgRotatePhoto = mView.findViewById(R.id.imgRotatePhoto);
        mImgRotatePhoto.setVisibility(View.VISIBLE);
        mImgFlipPhoto = mView.findViewById(R.id.imgFlipPhoto);
        mImgFlipPhoto.setVisibility(View.VISIBLE);
        mTextCrop = mView.findViewById(R.id.textCrop);
        mTextCrop.setVisibility(View.VISIBLE);

        mImgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        cropImageView = mView.findViewById(R.id.cropImageView);
        cropImageView.setAspectRatio(2, 2);
        cropImageView.setImageUriAsync(Uri.fromFile(new File(mSelectedImagePath)));

        mImgRotatePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });

        mImgFlipPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.flipImageHorizontally();
            }
        });

        mTextCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = cropImageView.getCroppedImage();
                String path = Utils.saveCroppedBitmap(bitmap);
                mActivity.goToPicEditFragment(path);
            }
        });

    }

}
