package com.group.ag.ag_group_tensorflow4android_v11.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.models.PhotoDTO;
import com.group.ag.ag_group_tensorflow4android_v11.utils.OnRecyclerItemClickListener;

import java.util.ArrayList;

public class StyledPhotosAdapter extends RecyclerView.Adapter<StyledPhotosAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<PhotoDTO> mData;
    private OnRecyclerItemClickListener mItemClickListener;

    public StyledPhotosAdapter(Context context, ArrayList<PhotoDTO> list) {
        this.mContext = context;
        this.mData = list;
    }

    public void setOnItemClickListener(final OnRecyclerItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                mContext).inflate(R.layout.row_styled_photo, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        PhotoDTO dto = mData.get(position);
        Glide.with(mContext)
                .load(Uri.parse("file://" + dto.imagePath)) // Uri of the picture
                .override(100, 100).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(holder.imgPhoto);
        holder.imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhoto;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgPhoto = (ImageView) itemView.findViewById(R.id.imgRecentPhoto);

        }
    }
}
