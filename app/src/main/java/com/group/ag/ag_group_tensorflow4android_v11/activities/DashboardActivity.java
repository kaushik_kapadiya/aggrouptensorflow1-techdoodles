package com.group.ag.ag_group_tensorflow4android_v11.activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.group.ag.ag_group_tensorflow4android_v11.R;
import com.group.ag.ag_group_tensorflow4android_v11.fragments.DashboardFragment;
import com.group.ag.ag_group_tensorflow4android_v11.fragments.MomentCameraFragment;
import com.group.ag.ag_group_tensorflow4android_v11.fragments.PicCropFragment;
import com.group.ag.ag_group_tensorflow4android_v11.fragments.PicEditFragment300160;
import com.group.ag.ag_group_tensorflow4android_v11.fragments.PicPreviewFragment;
import com.group.ag.ag_group_tensorflow4android_v11.utils.Constants;

public class DashboardActivity extends AppCompatActivity implements Constants {

    private Fragment mCurrentFragment;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        getSupportActionBar().hide();

        MobileAds.initialize(this, getResources().getString(R.string.admob_id));
        loadAd();

        addFragment(new DashboardFragment());

    }

    public void showAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            loadAd();
            mInterstitialAd.show();
        }
    }

    public InterstitialAd getAd() {
        return mInterstitialAd;
    }

    public void loadAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
        AdRequest adRequest = new AdRequest.Builder().build();
//        .addTestDevice("E74B5515BA987CB4BC8AD83A64562087")
        mInterstitialAd.loadAd(adRequest);
    }

    public void goToCameraFragment() {
        Fragment fragment = new MomentCameraFragment();
        replaceFragment(fragment);
    }

    public void goToPicEditFragment(String imagePath) {
        Fragment prevFragment = getActiveFragment();
        if (prevFragment instanceof PicCropFragment) {
            getSupportFragmentManager().beginTransaction().remove(prevFragment).commit();
            getSupportFragmentManager().popBackStack();
        }
        prevFragment = getSupportFragmentManager().findFragmentByTag(PicEditFragment300160.class.getName());
        if (prevFragment != null && prevFragment instanceof PicEditFragment300160) {
            getSupportFragmentManager().beginTransaction().remove(prevFragment).commit();
            getSupportFragmentManager().popBackStack();
        }

        Fragment fragment = new PicEditFragment300160();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_KEY_IMAGE_PATH, imagePath);
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    public void goToPicCropFragment(String imagePath) {
        Fragment prevFragment = getActiveFragment();
        if (prevFragment instanceof MomentCameraFragment) {
            getSupportFragmentManager().beginTransaction().remove(prevFragment).commit();
            getSupportFragmentManager().popBackStack();
        }

        Fragment fragment = new PicCropFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_KEY_IMAGE_PATH, imagePath);
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    public void goToPicPreviewFragment(String imagePath) {
        Fragment fragment = new PicPreviewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_KEY_IMAGE_PATH, imagePath);
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    public void replaceFragment(Fragment fragment) {
        String backStackTag = fragment.getClass().getName();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.frameDashboard, fragment, backStackTag);
        fragmentTransaction.addToBackStack(backStackTag);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void addFragment(Fragment fragment) {
        removeAllFragmentFromBackstack();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (mCurrentFragment != null) {
            Fragment f = getSupportFragmentManager().findFragmentByTag(mCurrentFragment.getClass().getName());
            if (f != null) {
                fragmentTransaction.remove(f);
            }
        }
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.add(R.id.frameDashboard, fragment, fragment.getClass().getName());
        fragmentTransaction.commit();
        mCurrentFragment = fragment;
    }

    private void removeAllFragmentFromBackstack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    private Fragment getActiveFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

}
